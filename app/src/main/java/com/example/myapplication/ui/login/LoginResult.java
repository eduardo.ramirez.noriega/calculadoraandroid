package com.example.myapplication.ui.login;

import androidx.annotation.Nullable;

import java.math.BigDecimal;

/**
 * Authentication result : success (user details) or error message.
 */
class LoginResult {
    @Nullable
    private LoggedInUserView success;
    @Nullable
    private Integer error;
    @Nullable
    private Double resultado;

    @Nullable
    public Double getResultado() {
        return resultado;
    }

    public void setResultado(@Nullable Double resultado) {
        this.resultado = resultado;
    }

    LoginResult(@Nullable Integer error) {
        this.error = error;
    }

    LoginResult(@Nullable LoggedInUserView success, Double resultado) {
        this.success = success;
        this.resultado = resultado;
    }

    @Nullable
    LoggedInUserView getSuccess() {
        return success;
    }

    @Nullable
    Integer getError() {
        return error;
    }
}